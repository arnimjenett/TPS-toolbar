# TPS-toolbar 
a set of fiji macros, bundled into an [ActionBar](https://imagejdocu.tudor.lu/doku.php?id=plugin:utilities:action_bar:start), for accereating access to frequently used tools. 

As a scaffold for quick access to build-in functions and custom-made macros it is expected to change over time - reacting on the needs of the labs of the UAR TEFOr Paris-Saclay. If you want to make sure, your version does not change, or changes in a way suitable to your lab, please feel free to fork this project.

The initial implementation was inteded for the work with a big (and growing) collection of big 3D images acquired with Confocal or multiphoton microscopy and organized by structured filenames within the 'file system based data base ([fsdb](https://gitlab.com/arnimjenett/fsdb)).

## Installation
- download this repository (e.g. as .zip)
- unpack the downloaded data 
  - this will result in a folder 'Fiji.app', which contains the same structure as fiji comes with natively.
- shutdown all instances of the fiji you want to install the TPS-toolbar to.
- copy-and-paste the new 'Fiji.app' folder on the 'Fiji.app' folder of your local fiji-installation. 
- if you get warnings, that you may overwrite files, allow this, please.
-> When you restart the modified fiji, the TPS-toolbar will open up at startup. 

## Acknowledgement
This tool (as well as the [TPS-annobar](https://gitlab.com/arnimjenett/TPS-annobar) are based on [Jerome Mutterer](http://www.ibmp.cnrs.fr/annuaire/jerome-mutterer/)'s [ActionBar](https://imagejdocu.tudor.lu/doku.php?id=plugin:utilities:action_bar:start). 
