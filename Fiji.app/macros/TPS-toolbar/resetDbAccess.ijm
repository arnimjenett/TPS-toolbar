tool=call("ij.Prefs.get", "tcf.disp.tool", "toolbar")

stackTypes=newArray("lif", "nd2", "czi");
dataFormats=newArray("mip", "aip", "cs", "mp4", "org");
optFormats=newArray("sc", "oc", "lc");
BOOLEANS=newArray("AND", "OR", "NOT")

resetIndex();
resetDefaults();

function resetIndex(){
// concept: the memoryFile conserves the settings from one session to the next. 	
	memoryFile=getDirectory("temp")+"mipDisplay.mem.txt";
// if memoryFile doesn't exist, yet, make one.	
	if (File.exists(memoryFile) == 0){
		fid=File.open(memoryFile);
		File.close(fid);
	}
// read content of memoryFile	
	mf=File.openAsString(memoryFile);
	mf=split(mf, "\n");
//	ans=getBoolean("Changing index?\n("+memoryFile+")");
	ans=getBoolean("Current values:\nindex: "+mf[0]+"\nmount point: "+mf[1]+"\nChanging index?");
	if ( ans == 1 ) {
		print("Resetting\n"+memoryFile);
		if (File.exists(memoryFile)) {
			File.delete(memoryFile);
		} 
		mf=File.open(memoryFile);
		indexPath=getDirectory("select index directory");
		call("ij.Prefs.set", "tcf.disp."+tool+".indexPath", indexPath);
		print(mf, indexPath);
		mountPoint=getDirectory("select mount point of data server");
		call("ij.Prefs.set", "tcf.disp."+tool+".mountPoint", mountPoint);
		print(mf, mountPoint);
		File.close(mf);
	} else {
		print("Change of index canceled.");
	}
}

function resetDefaults(){
	ans=getBoolean("Resetting search terms?");
	if ( ans == 1 ) {
		call("ij.Prefs.set", "tcf.disp."+tool+".SS1", "");
		call("ij.Prefs.set", "tcf.disp."+tool+".SS2", "");
		call("ij.Prefs.set", "tcf.disp."+tool+".SS3", "");
		
		for (i=0;i<lengthOf(stackTypes);i++){
			call("ij.Prefs.set", "tcf.disp."+tool+"."+stackTypes[i], 0);
		}
		for (i=0;i<lengthOf(dataFormats);i++){
			call("ij.Prefs.set", "tcf.disp."+tool+".modus."+dataFormats[i], 0);
		}
		for (i=0;i<lengthOf(optFormats);i++){
			call("ij.Prefs.set", "tcf.disp."+tool+".modus."+optFormats[i], 0);
		}
		for (i=1;i<=lengthOf(BOOLEANS);i++){
			call("ij.Prefs.set", "tcf.disp."+tool+".BO"+i, BOOLEANS[1]);
		}
	} else {
		print("Reset of defaults canceled.");
	}		
}


