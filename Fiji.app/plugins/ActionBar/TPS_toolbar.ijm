// Action Bar description file : TPS_toolbar.ijm

<startupAction>
print("Started TPS_toolbar");
fs=File.separator;
call("ij.Prefs.set", "tcf.anno.query.ijm",  getDirectory("macros")+"TPS-toolbar/dbAccess.ijm");
call("ij.Prefs.set", "tcf.anno.query.ijm",  getDirectory("macros")+"TPS-toolbar/resetDbAccess.ijm");
call("ij.Prefs.set", "tcf.disp.tool", "toolbar")
</startupAction>

<DnDAction>
file=getArgument();
param = "open=[" + file + "]";
run("Bio-Formats Importer", param);
</DnDAction>

<line>
<button> 1 line 1
label=import
icon=icons/TPS/loci-import.png
print(icon);
arg=<macro>
run("Bio-Formats Importer");
</macro>

<button> 2 line 1
label=openH5
icon=icons/TPS/oH5-32.png
arg=<macro>
run("Open XML/HDF5");
</macro>

<button> 3 line 1
label=CT
icon=icons/TPS/RGB32.png
arg=<macro>
run("Channels Tool...");
</macro>

<button> 4 line 1
label=CLAHE
icon=icons/TPS/clahe32.png
arg=<macro>
run("Enhance Local Contrast (CLAHE)", "blocksize=127 histogram=256 maximum=3 mask=*None* fast_(less_accurate)");
</macro>

<button> 5 line 1
label=Rec
icon=icons/TPS/rec32.png
arg=<macro>
run("Record...");
</macro>

<button> 6 line 1
label=empty
icon=icons/TPS/empty.png
arg=<macro>
print("no function associated with this button"); 
call("java.lang.System.gc");
//	run("Action Bar", "/plugins/ActionBar/TPS_annobar.ijm");
</macro>


<button> 7 line 1
label=TPS-loader
icon=icons/TPS/db32.png
arg=<macro>
call("ij.Prefs.set", "tcf.disp.tool", "toolbar")
runMacro(call("ij.Prefs.get", "tcf.anno.query.ijm",  getDirectory("macros")+"TPS-toolbar/dbAccess.ijm"))
</macro>

</line>

// LINE 2:

<line>
<button> 1 line 2
label=export
icon=icons/TPS/loci-export.png
arg=<macro>
run("Bio-Formats Exporter");
</macro>

<button> 2 line 2
label=exportH5
icon=icons/TPS/sH5-32.png
arg=<macro>
title=getTitle();
bn=split(title, ".");
bn=bn[0];
dir=File.directory;
fs=File.separator;
outFile=dir+fs+bn+".xml"
print(outFile);
run("Export Current Image as XML/HDF5", "  subsampling_factors=[{ {1,1,1}, {2,2,2} }] hdf5_chunk_sizes=[{ {16,16,16}, {16,16,16} }] value_range=[Use values specified below] min=0 max=65535 timepoints_per_partition=0 setups_per_partition=0 use_deflate_compression export_path="+outFile)
</macro>

<button> 3 line 2
label=B&C
icon=icons/TPS/BC32.png
arg=<macro>
run("Brightness/Contrast...");
</macro>

<button> 4 line 2
label=png
icon=icons/TPS/sPNG32.png
arg=<macro>
title=getTitle();
bn=split(title, ".");
bn=bn[0];
if (File.isDirectory(File.directory) == 1 ) {
	dir=File.directory;
} else {
	dir=getDirectory("home");
}
saveAs("PNG", dir+File.separator+bn+".png");
print("Saved as "+dir+File.separator+bn+".png");
</macro>

<button> 5 line 2
label=annotate
icon=icons/TPS/anno.png
arg=<macro>
call("java.lang.System.gc");
if ( File.exists(getDirectory("plugins")+"ActionBar/TPS_annobar.ijm" )){
	run("Action Bar", "/plugins/ActionBar/TPS_annobar.ijm");
} else {
	showMessage("TPS-annobar is missing", "<html>"+"<p>Please consider downloading it from our <a href=https://gitlab.com/arnimjenett/tps-annobar>gitlab repository</a>.");
}
</macro>

<button> 6 line 2
label=organise open images
icon=icons/TPS/tile32.png
arg=<macro>
if ( nImages > 0 ){
	run("Tile");
}
</macro>

<button> 7 line 2
label=reset TPS-loader
icon=icons/TPS/resetdb32.png
arg=<macro>
call("ij.Prefs.set", "tcf.disp.tool", "toolbar")
runMacro(call("ij.Prefs.get", "tcf.anno.query.ijm", getDirectory("macros")+"TPS-toolbar/resetDbAccess.ijm"))
</macro>

</line>

// end of file
